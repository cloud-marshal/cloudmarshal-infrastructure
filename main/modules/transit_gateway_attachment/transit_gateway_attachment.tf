resource "aws_ec2_transit_gateway_vpc_attachment" "transit_gateway_attachment" {
  transit_gateway_id = var.transit_gateway_id
  vpc_id             = var.vpc_id
  subnet_ids         = var.subnet_ids
  dns_support        = true
  ipv6_support       = false

  tags = {
    Name = "TransitGatewayAttachment"
  }
}