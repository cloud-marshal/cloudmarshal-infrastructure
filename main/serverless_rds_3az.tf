provider "aws" {
  region = "us-east-1"  # Replace with your desired region
}

module "mysql_serverless_rds" {
  source = "./modules/mysql_serverless_rds"
  
  availability_zones = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]  # Replace with your desired availability zones
  
  db_name            = "cloudmarshal_users"
  db_username        = "admin"
  db_password        = "password123"
  instance_size      = "db.t2.small"
}