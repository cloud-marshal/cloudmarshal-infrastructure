# Define the VPC module
module "vpc_module" {
  source = "./modules/vpc"

  cidr_block = var.vpc_cidr
}

# Define the subnet module
module "subnet_module" {
  source = "./modules/subnet"

  availability_zones = var.availability_zones
  vpc_id             = module.vpc_module.vpc_id
  subnet_cidr_base   = var.subnet_cidr_base
}

# Define the transit gateway attachment
module "transit_gateway_module" {
  source = "./modules/transit_gateway_attachment"

  transit_gateway_id = var.transit_gateway_id
  vpc_id             = module.vpc_module.vpc_id
  subnet_ids         = module.subnet_module.subnet_ids
}